"""
solution to K missing numbers in an unsorted array of numbers, 
including negative numbers (bonus) 

author: Richard Mutezintare (transmeta01@gmail.com)
"""

import time


def find_missing_numbers(arr):
    arr_set = set(arr)
    min_num = min(arr)
    max_num = max(arr)
    missing = [num for num in range(
        min_num, max_num + 1) if num not in arr_set]

    return missing


arr = [3, 4, -3, 6, 7, 8, 9, 6, 10, 100, 17, 23]
print("Input list:", arr)
print("Length of input list is:", len(arr) + 1)

start_time = time.time()
missing = find_missing_numbers(arr)

print("The missing values are:", missing)

print(
    f"excution time is {(time.time() - start_time) * 1_000_000:.4f} microseconds")
